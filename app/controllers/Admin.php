<?php

namespace Controller;

use Wolff\Core\Container;
use Wolff\Core\View;
use Wolff\Core\Session;

class Admin extends \Wolff\Core\Controller
{

    /**
     * Posts list.
     */
    public static function list()
    {
        View::render('admin/post_list', [
            'posts' => Container::get('db')->query("SELECT p.*, c.name as category FROM post p
                INNER JOIN category c ON p.category_id = c.category_id
                ORDER BY post_id DESC")->get(),
        ]);
    }


    /**
     * Edit post.
     */
    public static function edit($req, $res)
    {
        $id = $req->query('id') ?? 0;
        $post = Container::get('db')->select('post', 'post_id = ?', $id)[0] ?? null;
        View::render('admin/post_form', [
            'id'         => $id,
            'post'       => $post ?? [],
            'editing'    => isset($post),
            'categories' => Container::get('db')->select('category'),
        ]);
    }


    /**
     * Save post.
     */
    public static function save($req, $res)
    {
        $id = $req->query('id') ?? 0;
        $db = Container::get('db');

        if ($db->count('post', 'post_id = ?', $id) > 0) {
            $db->query('UPDATE post
                SET title = ?, description = ?, category_id = ?, content = ?, image = ?, status = ?, date = ?
                WHERE post_id = ?',
                $req->body('title'), $req->body('description'), $req->body('category_id'), $req->body('content'), $req->body('image'), $req->body('status'), $req->body('date'), $id);
        } else {
            $db->insert('post', [
                'title'       => $req->body('title'),
                'description' => $req->body('description'),
                'category_id' => $req->body('category_id'),
                'content'     => $req->body('content'),
                'image'       => $req->body('image'),
                'status'      => $req->body('status'),
                'date'        => $req->body('date'),
            ]);
        }

        redirect('list');
    }


    /**
     * Delete post.
     */
    public static function delete($req, $res)
    {
        Container::get('db')->query('DELETE FROM post
            WHERE post_id = ?', $req->query('id') ?? 0);
        redirect('list');
    }


    /**
     * Login.
     */
    public static function login($req, $res)
    {
        Session::start();
        Session::unset('login');
        View::render('admin/login');
    }


    /**
     * Enter admin.
     */
    public static function start($req, $res)
    {
        if ($req->body() == config('admin')) {
            Session::set('login', true);
        }

        redirect('list');
    }
}
