<?php

namespace Controller;

use Wolff\Core\Container;
use Wolff\Core\View;

class Home extends \Wolff\Core\Controller
{

    const WPM = 210;
    const EMAIL = 'contacto@usbac.com.ve';
    const PER_PAGE = 6;
    const SIDE_PAGES = 2;
    const PROJECTS = [
        [
            'title'       => 'Wolff',
            'img'         => 'wolff.png',
            'url'         => 'https://github.com/usbac/wolff',
            'language'    => 'PHP',
            'description' => 'Framework ligero y rapido para construir paginas web.',
            'version'     => '3.2.2',
        ],
        [
            'title'       => 'Tundra',
            'img'         => 'tundra.png',
            'url'         => 'https://github.com/usbac/tundra',
            'language'    => 'Javascript',
            'description' => 'Motor de plantillas personalizable para NodeJS.',
            'version'     => '2.0.1',
        ],
        [
            'title'       => 'Quich',
            'img'         => 'quich.png',
            'url'         => 'https://github.com/usbac/quich',
            'language'    => 'ANSI C',
            'description' => 'Calculadora con opciones avanzadas para el terminal.',
            'version'     => '3.0.0',
        ],
        [
            'title'       => 'Nmly',
            'img'         => 'nmly.png',
            'url'         => 'https://github.com/usbac/nmly',
            'language'    => 'ANSI C',
            'description' => 'Renombrador masivo y configurable de archivos.',
            'version'     => '1.1.1',
        ],
    ];


    /**
     * Home page.
     */
    public function index($req, $res)
    {
        View::render('home', [
            'page'     => 'Home',
            'projects' => self::PROJECTS,
            'posts'    => self::getPosts('', 0),
        ]);
    }


    /**
     * Blog page.
     */
    public function blog($req, $res)
    {
        $search = $req->query('search') ?? '';
        $pagination = new \Wolff\Utils\Pagination(
            self::getTotalPosts($search),
            self::PER_PAGE,
            $req->query('page') ?? 1,
            self::SIDE_PAGES,
            url("blog?page={page}&search=$search")
        );

        View::render('blog', [
            'page'             => 'Blog',
            'posts_pagination' => $pagination->get(),
            'posts'            => self::getPosts($search, $req->query('page')),
            'search'           => $search,
        ]);
    }


    /**
     * Contact page.
     */
    public function contact()
    {
        View::render('contact', [
            'page' => 'Contact',
        ]);
    }


    /**
     * Post page.
     */
    public function post($req, $res)
    {
        $post = Container::get('db')->query("SELECT p.*, c.name as category FROM post p
            INNER JOIN category c ON c.category_id = p.category_id
            WHERE p.status = 1 AND p.post_id = ?", $req->query('id') ?? 0)->first();

        if (empty($post)) {
            return $res->setHeader('Location', url());
        }

        // Remove current post from other posts list
        $others = self::getPosts(null, 1);
        foreach ($others as $key => $val) {
            if ($val['post_id'] == $req->query('id')) {
                unset($others[$key]);
            }
        }

        View::render('post', [
            'post_id'     => $post['post_id'],
            'title'       => $post['title'],
            'description' => $post['description'],
            'category'    => $post['category'],
            'image'       => $post['image'],
            'date'        => self::getDate($post['date']),
            'html'        => (new \cebe\markdown\GithubMarkdown())->parse($post['content']),
            'time'        => floor(str_word_count($post['content']) / self::WPM),
            'others'      => array_slice($others, 0, 3),
        ]);
    }


    /**
     * Send email.
     */
    public function email($req, $res)
    {
        $res->setHeader('Content-type', 'application/json');

        foreach ($req->body() as $val) {
            if (empty($val)) {
                return $res->write(json_encode([ 'success' => false ]));
            }
        }

        mail(self::EMAIL, $req->body('title'), $req->body('email') . "\n" . $req->body('content'));

        $res->write(json_encode([ 'success' => true ]));
    }


    private static function getPosts($search = '', $page = 1)
    {
        $search = '%' . $search . '%';
        $posts = Container::get('db')->query("SELECT p.*, c.name as category FROM post p
            INNER JOIN category c ON p.category_id = c.category_id
            WHERE p.status = 1 AND (p.title LIKE ? OR p.description LIKE ?)
            ORDER BY date DESC
            LIMIT " . ($page - 1) * self::PER_PAGE . ", " . self::PER_PAGE, $search, $search)->get();

        // Format posts
        foreach ($posts as $key => $val) {
            $posts[$key]['date'] = self::getDate($val['date']);
            $posts[$key]['time'] = floor(str_word_count($val['content']) / self::WPM);
        }

        return $posts;
    }


    private static function getTotalPosts($search = '')
    {
        $search = '%' . $search . '%';

        return Container::get('db')->query("SELECT * FROM post p
            INNER JOIN category c ON p.category_id = c.category_id
            WHERE p.status = 1 AND (p.title LIKE ? OR p.description LIKE ?)", $search, $search)->count();
    }


    private static function getDate($date)
    {
        return strtr(strftime('%e %B %Y', strtotime($date)), [
            'January'   => 'Ene',
            'February'  => 'Feb',
            'March'     => 'Mar',
            'April'     => 'Abr',
            'May'       => 'May',
            'June'      => 'Jun',
            'July'      => 'Jul',
            'August'    => 'Ago',
            'September' => 'Sep',
            'October'   => 'Oct',
            'November'  => 'Nov',
            'December'  => 'Dic',
        ]);
    }
}
