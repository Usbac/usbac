<?php

define('WOLFF_CONFIG', [
    'version'  => '4.0.0',
    'csrf_key' => '__token',
    'start'    => microtime(true),
]);
