<?php

use Wolff\Core\Route;
use Wolff\Core\View;
use Wolff\Core\Container;
use Wolff\Core\DB;
use Wolff\Core\Middleware;
use Wolff\Core\Session;

Route::get('/', [ Controller\Home::class, 'index' ]);
Route::get('blog', [ Controller\Home::class, 'blog' ]);
Route::get('post/{id}', [ Controller\Home::class, 'post' ]);
Route::get('contact',  [ Controller\Home::class, 'contact' ]);
Route::post('sendEmail', [ Controller\Home::class, 'mail' ]);

// Admin

Route::get('admin', [ Controller\Admin::class, 'login' ]);
Route::get('admin/list', [ Controller\Admin::class, 'list' ]);
Route::post('admin/start', [ Controller\Admin::class, 'start' ]);
Route::get('admin/edit/{id?}', [ Controller\Admin::class, 'edit' ]);
Route::get('admin/delete/{id}', [ Controller\Admin::class, 'delete' ]);
Route::post('admin/save', [ Controller\Admin::class, 'save' ]);

// Extra

Route::code(404, function () {
    View::render('404');
});

Container::singleton('db', function() {
    return new DB;
});

Middleware::before('admin/*', function ($req, $next) {
    Session::start();

    if (Session::get('login') !== true && getCurrentPage() != '/admin/start') {
        http_response_code(404);
        header('Location:' . url('admin'));
    }
});
